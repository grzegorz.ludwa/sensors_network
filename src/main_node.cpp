#include <boost/circular_buffer.hpp>
#include <iostream>
#include <zmqpp/zmqpp.hpp>


std::string buffer_to_string(const boost::circular_buffer<std::string> &buffer) {
  std::string result;
  for(size_t i = 0; i < buffer.size(); ++i) {
    result += i;
    result += ": ";
    result += buffer[i];
  }
  return result;
}

void forward_messages(zmqpp::context &context, size_t buffer_size) {
  boost::circular_buffer<std::string> buffer(buffer_size);

  zmqpp::socket publisher(context, zmqpp::socket_type::publish);
  publisher.bind("tcp://*:5556");

  zmqpp::socket subscriber(context, zmqpp::socket_type::subscribe);
  subscriber.bind("tcp://*:5555");
  subscriber.subscribe("");

  while (true) {
    std::string topic;
    std::string message;
    subscriber.receive(topic);
    subscriber.receive(message);
    std::string full_message = "Topic: " + topic + ", message: " + message + "\n";
    std::cout << "Received: " << full_message << "\n";

    buffer.push_back(full_message);
    publisher.send(buffer_to_string(buffer));
  }
}

int main() {
  zmqpp::context context;
  size_t buffer_size = 10;
  forward_messages(context, buffer_size);
  return 0;
}
