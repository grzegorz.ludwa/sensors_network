#include <random>
#include <sensors_network/sensor.h>
#include <sensors_network/temperature_msg.h>
#include <zmqpp/zmqpp.hpp>

double generateRandomDouble(double min, double max) {
  static std::random_device random_device;
  static std::mt19937 gen(random_device());

  std::uniform_real_distribution<> dis(min, max);

  return dis(gen);
}

class TemperatureSensor : public Sensor<TemperatureMsg> {
public:
  TemperatureSensor(std::string frame_id, std::string topic,
                    std::string endpoint, zmqpp::context &context)
      : Sensor<TemperatureMsg>(std::move(frame_id), std::move(topic),
                               std::move(endpoint), context) {}
  TemperatureMsg read() override {
    HeaderMsg header(std::time(nullptr), frame_id_);
    double temperature = generateRandomDouble(0, 25);
    return TemperatureMsg(header, temperature);
  }
};

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " <sensor_id>\n";
    return 1;
  }

  std::string sensor_id = argv[1];

  zmqpp::context context;
  std::string endpoint = "tcp://localhost:5555";

  TemperatureSensor sensor(std::string("sensor").append(sensor_id), sensor_id, endpoint, context);
  while (true) {
    TemperatureMsg msg = sensor.read();
    sensor.send(msg);
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
}
