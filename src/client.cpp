#include <iostream>
#include <zmqpp/zmqpp.hpp>

int main() {
  zmqpp::context context;
  std::string client_endpoint = "tcp://localhost:5556";

  zmqpp::socket subscriber(context, zmqpp::socket_type::subscribe);
  subscriber.connect(client_endpoint);
  subscriber.subscribe("");

  while (true) {
    std::string message;
    subscriber.receive(message);

    std::cout << "Current buffer: " << message << "\n";
  }
  return 0;
}
