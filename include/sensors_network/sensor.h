#ifndef SENSOR_H
#define SENSOR_H

#include <string>
#include <zmqpp/zmqpp.hpp>

template <typename T> class Sensor {
public:
  Sensor(const Sensor &) = delete;
  Sensor(Sensor &&) = delete;
  Sensor &operator=(const Sensor &) = delete;
  Sensor &operator=(Sensor &&) = delete;

  Sensor(std::string frame_id, std::string sensor_id, std::string endpoint,
         zmqpp::context &context)
      : frame_id_(std::move(frame_id)), sensor_id_(std::move(sensor_id)),
        endpoint_(std::move(endpoint)),
        publisher_(context, zmqpp::socket_type::publish) {
    publisher_.connect(endpoint_);
  }
  virtual ~Sensor() = default;
  virtual T read() = 0;
  void send(T msg) {
    zmqpp::message message;
    message << sensor_id_ << msg.to_string();
    publisher_.send(message);
  }

protected:
  std::string frame_id_;
  std::string sensor_id_;
  std::string endpoint_;
  zmqpp::socket publisher_;
};

#endif // SENSOR_H
