#ifndef MSG_H
#define MSG_H

#include <string>

class Msg {
public:
  Msg() = default;
  Msg(const Msg &) = default;
  Msg(Msg &&) = default;
  Msg &operator=(const Msg &) = default;
  Msg &operator=(Msg &&) = default;
  virtual ~Msg() = default;

  // *to_string* should be replaced with serialization library (eg. protobuf)
  [[nodiscard]] virtual std::string to_string() const = 0;
};

#endif // MSG_H
