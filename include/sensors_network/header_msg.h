#ifndef HEADER_MSG_H
#define HEADER_MSG_H

#include <ctime>
#include <sensors_network/msg.h>
#include <string>

class HeaderMsg : public Msg {
public:
  std::time_t timestamp{0};
  std::string frame_id{"base"};

  HeaderMsg(std::time_t timestamp, std::string frame_id)
      : timestamp(timestamp), frame_id(std::move(frame_id)) {}

  [[nodiscard]] std::string to_string() const override {
    return "HeaderMsg{timestamp: " + std::to_string(timestamp) +
           ", frame_id: " + frame_id + "}";
  }
};

#endif // HEADER_MSG_H
