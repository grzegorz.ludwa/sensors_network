#ifndef TEMPERATURE_MSG_H
#define TEMPERATURE_MSG_H

#include <sensors_network/header_msg.h>
#include <sensors_network/msg.h>

class TemperatureMsg : public Msg {
public:
  HeaderMsg header;
  double temperature{};

  explicit TemperatureMsg(HeaderMsg header, double temperature)
      : header(std::move(header)), temperature(temperature) {}

  [[nodiscard]] std::string to_string() const override {
    return "TemperatureMsg{header: " + header.to_string() +
           ", temperature: " + std::to_string(temperature) + "}";
  }
};

#endif // TEMPERATURE_MSG_H
