from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout, CMakeDeps


class sensorsNetworkRecipe(ConanFile):
    name = "sensors_network"
    version = "0.1"
    package_type = "application"

    tool_requires = "cmake/3.29.6"
    requires = "zmqpp/4.2.0", "boost/1.85.0"

    # Optional metadata
    author = "Grzegorz Ludwa <grzegorz.ludwa@wp.pl>"
    description = "This is a hello world application"

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*", "include/*"

    def layout(self):
        cmake_layout(self)

    def generate(self):
        deps = CMakeDeps(self)
        deps.generate()
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
