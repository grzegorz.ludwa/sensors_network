# sensors_network

This is implementation of exercise send by a recruiter.

## Usage

0. Install `conan`

```console
python3 -m venv venv
. venv/bin/activate
pip install conan
```

1. Install conan dependencies and build.

```console
conan install -pr profiles/x86_64-gcc12-cxx17-Release . --build=missing
conan build -pr profiles/x86_64-gcc12-cxx17-Release .
```

2. Now open (at least) 4 different consoles.

In the first console run the main node: `./build/Release/main_node`

Then in a few different consoles you can run different temperature sensors: `./build/Release/temperature_sensor <sensor_id>`, eg. `./build/Release/temperature_sensor temp1`.

In the last console you can run the client, which will show currently saved buffer in main node: `./build/Release/client`.

Example output:
![example](./img/example.png)
